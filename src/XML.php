<?php

namespace Pashynskyi\XMLBuilder;

class XML
{
    protected $xml;

    public function __construct(array $attributes = ['version' => '1.0', 'encoding' => 'UTF-8'])
    {
        $this->xml = '<?xml' . Helper::attributesToString($attributes) . '?>' . PHP_EOL;
        return $this;
    }

    public function raw($rawString)
    {
        $this->xml .= $rawString . PHP_EOL;
    }

    public function tag(Tag $tag)
    {
        $this->xml .= $tag;
    }

    public function __toString()
    {
        return $this->xml;
    }
}
