<?php

namespace Pashynskyi\XMLBuilder;

class Tag
{
    protected $tag;

    public function __construct($name, $value = null, array $attributes = [])
    {
        $this->tag = '<' . $name . Helper::attributesToString($attributes);
        if (isset($value)) {
            $this->tag .= '>';
            if (is_array($value)) {
                $this->tag .= PHP_EOL;

                foreach ($value as $tag) {
                    if (is_array($tag)) {
                        foreach ($tag as $t) {
                            $this->tag .= $t;
                        }
                    } else {
                        $this->tag .= $tag;
                    }
                }
            }
            else {
                $this->tag .= $value;
            }
            $this->tag .= "</$name>";
        }
        else {
            $this->tag .= '/>';
        }
        $this->tag .= PHP_EOL;

        return $this;
    }

    public function __toString()
    {
        return $this->tag;
    }
}
