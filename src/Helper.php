<?php

namespace Pashynskyi\XMLBuilder;

class Helper
{
    public static function attributesToString(array $attributes)
    {
        $attr = [];
        foreach ($attributes as $key => $value) {
            $attr[] = $key . '="' . $value . '"';
        }
        $sep = !empty($attr) ? ' ' : '';
        return $sep . implode(' ', $attr);
    }

    public static function cData($string)
    {
        return '<![CDATA[' . $string . ']]>';
    }
}
